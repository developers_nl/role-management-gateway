# Role for setting up a management gateway
Allows forwarding traffic from an internal network interface to an external
network interface, enabling internal servers to access the internet.

The module also makes the iptable rules persistent.

## Required variables

* `internal_interface` - The name of the internal network interface
* `external_interface` - The name of the external network interface

## Supported distributions
 * Debian
 * Ubuntu
 * CentOS
 * RHEL
